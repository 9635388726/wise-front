**POST - /login**
```javascript
{
    email: String,
    password: String,
}
```
**↪️ return**
```javascript
{
    userEmail: String,
    etablissement: String,
    role: String,
    userToken: String,
}
```
--------------------------------------------------------------------------------------
**GET - /multi/formation**
```javascript
{
    token: String,
    // Filtres à définir pour récupérer uniquement certaines formations
    // ↪️ renvoyer une liste triée par l'établissement, le lieu, ou si il y a une certification
}
```
**↪️ return**
```javascript
[
            {
                id: INT
            },
            {
                id: INT
            },
            {
                id: INT
            },
            {
                id: INT
            },
            {
                id: INT
            }
            ect ...
        ]
```
--------------------------------------------------------------------------------------
**GET - /single/formation**
```javascript
{
    token: String,
    id: String,
}
```
**↪️ return**
```javascript
{
    id: INT,
    formationName: String,
    formationDescription: String,
    formationStartDate: String, // En date ISO
    formationDuration: INT, // En heures
    etablissementName: String,
    etablissementAdressPostcode: INT,
    etablissementAdressCity: String,

    ↪️ Possible de mettre d'autres valeurs sous cette forme :

    moreInfos : [
        { valeur1: donnée1 },
        { valeur2: donnée2 },
        { valeur3: donnée3 },
        { valeur4: donnée4 },
        { valeur5: donnée5 },
    ]

}
```
--------------------------------------------------------------------------------------
**POST - /single/create/formation**
```javascript
{
    token: String,
    // DEV => Reste à définir
}
```
**↪️ return**
```javascript
200 - OK
```
--------------------------------------------------------------------------------------
**POST - /multi/create/formation**
```javascript
{
    token: String,
    formData: Media, // Fichier .xls à traiter
}
```
**↪️ return**
```javascript
200 - OK
```
--------------------------------------------------------------------------------------
**POST - /single/create/user**
```javascript
{
    token: String,
    email: String,
    password: String,
    etablissement: String,
    role: String,
}
```
**↪️ return**
```javascript
200 - OK
```
--------------------------------------------------------------------------------------
**GET - /single/user**
```javascript
{
    token: String
}
```
**↪️ return**
```javascript
{
     userEmail: String,
     etablissement: String,
     role: String,
}
```
--------------------------------------------------------------------------------------
**GET - /multi/user**
```javascript
{
    token: String
}
```
**↪️ return**
```javascript
[
    {
         userEmail: String,
         etablissement: String,
         role: String,
         createdAt: DATE ISO,
    },
    {
         userEmail: String,
         etablissement: String,
         role: String,
         createdAt: DATE ISO,
    },
    {
         userEmail: String,
         etablissement: String,
         role: String,
         createdAt: DATE ISO,
    },
    {
         userEmail: String,
         etablissement: String,
         role: String,
         createdAt: DATE ISO,
    },
    ect ...
]
```
--------------------------------------------------------------------------------------
**PUT - /update/password**
```javascript
{
    token: String,
    oldPassword: String,
    newPassword: String,
}
```
**↪️ return**
```javascript
200 - OK
```
--------------------------------------------------------------------------------------
**POST - /logout**
```javascript
{
    token: String,
}
```
**↪️ return**
```javascript
200 - OK
```