import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import { getAxios } from './getAxios';
import { parseError } from './handleError';
import Toasted from 'vue-toasted';


import VueSession from 'vue-session'
Vue.use(VueSession)

Vue.use(Toasted)

Vue.config.productionTip = false

Vue.mixin({
  methods: {
    getAxios,
    parseError
  },
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')