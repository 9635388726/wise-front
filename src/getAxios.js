import axios from 'axios';

function getAxios(token) {
  const baseUrl = process.env.VUE_APP_APISERVER;
  // Request axios.create, depends id you have a token or not
  if (token) {
    return axios.create({
      baseURL: baseUrl,
      timeout: 100000,
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  // eslint-disable-next-line no-else-return
  } else {
    return axios.create({
      baseURL: baseUrl,
      timeout: 100000,
      headers: {
      },
    });
  }
}

// eslint-disable-next-line import/prefer-default-export
export { getAxios };
